package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

	@Test
	public void testSimple() {
		assertCompiled(4.60, "2.55+2.05");
		assertCompiled(5, " 2 + 3 ");
		assertCompiled(1, "2 - 1 ");
		assertCompiled(7, " 2 * 3.5 ");
		assertCompiled(4.5, "  9 /2 ");
	}
    @Test
    public void testNullError(){
        try {assertCompiled(0,null);}
        catch (NullPointerException e){}
        assertCompiled(0,"");
    }

    @Test
    public void testEmptyStringError(){
        assertCompiled(0,"");
    }


	@Test
	public void testComplex() {
	//	assertCompiled(12, "  (2 + 2 ) * 3 ");
	//	assertCompiled(8.5, "  2.5 + 2 * 3 ");
	//	assertCompiled(8.5, "  2 *3 + 2.5");
	}
}
