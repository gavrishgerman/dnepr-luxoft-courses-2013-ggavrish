package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        if (input == null) {
            throw new NullPointerException("Input is NULL");
        }
        if (input.equals("")) {
            addCommand(result, VirtualMachine.PUSH, 0);
            addCommand(result, VirtualMachine.PRINT);
            return result.toByteArray();
        }

        input = input.replaceAll(" ", "");
        String[] numbers = getNumbers(input);
        String[] action = getActions(input);
        addCommand(result, VirtualMachine.PUSH, Double.parseDouble(numbers[0]));
        addCommand(result, VirtualMachine.PUSH, Double.parseDouble(numbers[1]));

        switch (action[1]) {
            case "+":
                sum(result);
                break;
            case "-":
                sub(result);
                break;
            case "/":
                div(result);
                break;
            case "*":
                mul(result);
                break;
        }
        addCommand(result, VirtualMachine.PRINT);

        return result.toByteArray();
    }


    /**
     * Method return what kind of action needs to be done.
     * Regex found on: http://gskinner.com/RegExr/
     * This regex matching all kinds of numbers(integer or double).
     *
     * @param input string
     * @return +-\* depends on a string.
     */
    public static String[] getActions(String input){
        return input.split("[0-9]+(?:\\.[0-9]*)?");
    }

    /**
     * Method takes all doubles of input string in array.
     * Regex found on: http://gskinner.com/RegExr/
     * This regex matching + or - or / or *
     * For example 2.44+4
     * returns 2.44 and 4
     *
     * @param input string.
     * @return all the numbers in the input string.
     */
    public static String[] getNumbers(String input){
        return input.split("\\+|\\-|\\*|/");
    }

    public static void sum(ByteArrayOutputStream result) {
        addCommand(result, VirtualMachine.ADD);
    }

    public static void sub(ByteArrayOutputStream result) {
        addCommand(result, VirtualMachine.SWAP);
        addCommand(result, VirtualMachine.SUB);
    }

    public static void div(ByteArrayOutputStream result) {
        addCommand(result, VirtualMachine.SWAP);
        addCommand(result, VirtualMachine.DIV);
    }

    public static void mul(ByteArrayOutputStream result) {
        addCommand(result, VirtualMachine.MUL);
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result  byte array.
     * @param command command.
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result  byte array.
     * @param command command.
     * @param value value.
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
