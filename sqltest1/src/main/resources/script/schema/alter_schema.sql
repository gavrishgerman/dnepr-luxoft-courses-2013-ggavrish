CREATE TABLE IF NOT EXISTS makers (
  maker_id INT AUTO_INCREMENT,
  maker_name VARCHAR(50) NOT NULL,
  maker_adress VARCHAR(200),
  CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_name, maker_adress) VALUES('A', 'AdressA');
INSERT INTO makers(maker_name, maker_adress) VALUES('B', 'AdressB');
INSERT INTO makers(maker_name, maker_adress) VALUES('C', 'AdressC');
INSERT INTO makers(maker_name, maker_adress) VALUES('D', 'AdressD');
INSERT INTO makers(maker_name, maker_adress) VALUES('E', 'AdressE');

ALTER TABLE product ADD COLUMN maker_id INT NOT NULL;
UPDATE product SET maker_id = 1 WHERE maker = 'A';
UPDATE product SET maker_id = 2 WHERE maker = 'B';
UPDATE product SET maker_id = 3 WHERE maker = 'C';
UPDATE product SET maker_id = 4 WHERE maker = 'D';
UPDATE product SET maker_id = 5 WHERE maker = 'E';
ALTER TABLE product DROP COLUMN maker;
ALTER TABLE product ADD CONSTRAINT fk_product_makers FOREIGN KEY(maker_id) REFERENCES makers(maker_id);


CREATE TABLE IF NOT EXISTS printer_type (
    type_id INT NOT NULL DEFAULT 0,
    type_name VARCHAR(50) NOT NULL,
    CONSTRAINT pk_printer_type PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name) VALUES(1, 'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES(2, 'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES(3, 'Matrix');


ALTER TABLE printer ADD COLUMN type_id INT NOT NULL;
UPDATE printer SET type_id = 1 WHERE type = 'Laser';
UPDATE printer SET type_id = 2 WHERE type = 'Jet';
UPDATE printer SET type_id = 3 WHERE type = 'Matrix';
ALTER TABLE printer DROP COLUMN type;
ALTER TABLE printer ADD CONSTRAINT fk_printer_type FOREIGN KEY(type_id) REFERENCES printer_type(type_id);

ALTER TABLE printer MODIFY color CHAR(1) NOT NULL;
ALTER TABLE printer ALTER COLUMN color SET DEFAULT 'y';

CREATE INDEX ind_pc_price ON pc (price ASC) USING BTREE;
CREATE INDEX ind_laptop_price ON laptop (price ASC) USING BTREE;
CREATE INDEX ind_printer_price ON printer (price ASC) USING BTREE;