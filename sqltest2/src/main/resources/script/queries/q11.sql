select maker_id, type from product
group by maker_id
having count(type)>1 and max(type) = min(type);
