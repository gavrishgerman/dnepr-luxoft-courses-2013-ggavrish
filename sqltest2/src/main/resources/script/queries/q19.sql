select maker_id, avg(hd) as 'avg_hd' from product
  join pc on product.model = pc.model
WHERE product.maker_id IN (SELECT maker_id FROM product WHERE type = 'printer')
GROUP BY maker_id;