Select distinct maker_name from product
  join makers on makers.maker_id = product.maker_id
where product.type = 'pc' and maker_name not in(
  select maker_name from product
    join makers on makers.maker_id = product.maker_id
  where product.type = 'laptop'
);