select DISTINCT maker_name from product
  join makers on makers.maker_id = product.maker_id
  join pc on product.model = pc.model
where pc.speed >= '450';
