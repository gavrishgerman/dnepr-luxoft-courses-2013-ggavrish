select type, product.model, laptop.speed from product
  join laptop on laptop.model = product.model
where speed<(select min(speed) from pc);