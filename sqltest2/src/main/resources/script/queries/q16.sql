select makers.maker_id, count(product.model) as 'mod_count'
from makers
  join product on product.maker_id = makers.maker_id
where type = 'PC'
group by makers.maker_id
having count(product.model)>2;