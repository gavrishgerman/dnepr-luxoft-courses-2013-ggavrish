select product.maker_id, avg(laptop.screen) as 'avg_size' from laptop
  join product on product.model = laptop.model
group by product.maker_id
order by avg(laptop.screen);