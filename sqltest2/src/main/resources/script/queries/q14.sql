select maker_name, min(printer.price) as 'price' from makers
  join product on makers.maker_id = product.maker_id
  join printer on product.model = printer.model
where color = 'y';