<!DOCTYPE html>
<html>

<head>

    <title>Welcome</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/styles/welcome.css" />
</head>

<body>
<div id="main">
    <a id="ref" href="http://localhost:8080/myFirstWebApp/user" method="post">logout</a>
    <h1 id="greetings">Hello ${name}!</h1>
</div>
</body>

</html>