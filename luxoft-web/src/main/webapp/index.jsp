<!DOCTYPE html>
<html>

<head>

    <title>Log In</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css" />

</head>

<body>
<div id="background">
    <form name="input" id="form" action="http://localhost:8080/myFirstWebApp/user" method="post">
        Username:<br>
        <input type="text" name="user"><br>
        Password:<br>
        <input type="password"  name="pass"><br>
        <input type="submit" id="btn" value="Sign In"><br>
    </form>
</div>
<div id="help">${error}</div>
</body>

</html>