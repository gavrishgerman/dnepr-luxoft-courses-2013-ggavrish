package com.luxoft.dnepr.courses.myFirstWebApp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User: Гера
 * Date: 15.12.13
 * Time: 13:32
 */
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        String name = request.getParameter("user");
        String pass = request.getParameter("pass");
        System.out.println(name);
        System.out.println(pass);

        if (name == null) {
            HttpSession session = request.getSession();
            session.invalidate();
            request.getRequestDispatcher("/WEB-INF/page.jsp").forward(request, response);
        } else {
            if (isValidUser(name, pass)) {
                HttpSession session = request.getSession();
                request.setAttribute("name", name); // This will be available as ${message}
                request.getRequestDispatcher("/WEB-INF/page.jsp").forward(request, response);
                session.setAttribute("name", request.getParameter("name"));
            } else {
                request.setAttribute("error", "Wrong login or password");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }

        }
    }



    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();
        session.invalidate();
        response.sendRedirect("/myFirstWebApp/index.html");
    }


    private boolean isValidUser(String name, String pass) {
        if (!UsersStorage.getData().containsKey(name)) {
            return false;
        }
        return UsersStorage.getData().get(name).equals(pass);
    }
}
