package com.luxoft.dnepr.courses.myFirstWebApp;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * User: Гера
 * Date: 15.12.13
 * Time: 13:31
 */
public class UsersStorage implements ServletContextListener{
    ServletContext context;
    private static Map<String, String> data = new HashMap<>();

    public void contextInitialized(ServletContextEvent contextEvent) {
        context = contextEvent.getServletContext();
        String userPath = context.getRealPath("/META-INF/" + context.getInitParameter("users"));
        deserializeFromXML(userPath);
    }

    public void contextDestroyed(ServletContextEvent contextEvent) {
        context = contextEvent.getServletContext();
    }

    public static Map<String, String> getData() {
        return Collections.unmodifiableMap(data);
    }

    private static void addEntry(NamedNodeMap attrs) {
        String name = attrs.item(0).getNodeValue();
        String password = attrs.item(1).getNodeValue();
        data.put(name, password);
    }

    private static void deserializeFromXML(String absolutePathToFile) {
        try {
            File file = new File(absolutePathToFile);
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    addEntry(node.getAttributes());
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

}
