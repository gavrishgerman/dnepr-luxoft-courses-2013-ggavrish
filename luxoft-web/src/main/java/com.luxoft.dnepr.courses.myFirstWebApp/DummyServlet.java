package com.luxoft.dnepr.courses.myFirstWebApp;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * User: Гера
 * Date: 08.12.13
 * Time: 20:39
 */

public class DummyServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String reqParamName = req.getParameter("name");
        String reqParamAge = req.getParameter("age");
        if (reqParamName == null || reqParamAge == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            ServletContext ctx = req.getServletContext();
            if (ctx.getAttribute(reqParamName) != null) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + reqParamName + " already exists\"}");
            } else {
                synchronized (this) {
                    ctx.setAttribute(reqParamName, reqParamAge);
                }
                resp.setStatus(201);
                writer.print("Created");
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String reqParamName = req.getParameter("name");
        String reqParamAge = req.getParameter("age");
        if (reqParamName == null || reqParamAge == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            ServletContext ctx = req.getServletContext();
            if (ctx.getAttribute(reqParamName) == null) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + reqParamName + " does not exist\"}");
            } else {
                synchronized (this) {
                    ctx.setAttribute(reqParamName, reqParamAge);
                }
                resp.setStatus(202);
                writer.print("{\"" + reqParamName + "\":\"" + reqParamAge + "\"}");
            }
        }
    }
}