package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;

public class EqualSet<E> implements Set<E> {
    private List<E> collection;

    public EqualSet() {
        this.collection = new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        this();
        for (E e : collection) {
            add(e);
        }
    }

    @Override
    public boolean contains(Object o) {
        return collection.contains(o);
    }

    @Override
    public void clear() {
        this.collection.clear();
    }

    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }


    @Override
    public boolean add(E o) {
        return !collection.contains(o) && collection.add(o);
    }

    @Override
    public Object[] toArray() {
        return collection.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return collection.toArray(a);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return collection.retainAll(c);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return collection.containsAll(c);
    }

    @Override
    public boolean remove(Object o) {
        return collection.remove(o);
    }

    @Override
    public Iterator<E> iterator() {
        return collection.iterator();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return collection.removeAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        boolean isChanged = true;
        for (E e : collection) {
            isChanged = add(e);
        }
        return isChanged;
    }

    @Override
    public int size() {
        return collection.size();
    }

}