package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;


public class UserInterface {
    Vocabulary vocabulary;

    public UserInterface() {
        this.vocabulary = new Vocabulary();
    }

    private void run() {
        boolean process = true;
        while (process) {
            String word = vocabulary.getRandomWord();
            getQuestion(word);
            Scanner in = new Scanner(System.in);
            if (in.hasNext()) {
                String input = in.next();
                process = inputMatching(input, word);
            } else {
                getErrorMessage();
                process = false;
            }
        }
    }

    private boolean inputMatching(String input, String word) {
        switch (input) {
            case "exit": {
                printResult(WordsStorage.getResults(vocabulary.getSize()));
                return false;
            }
            case "help": {
                getHelp();
            }
            default: {
                Boolean answer = input.equalsIgnoreCase("y");
                WordsStorage.answer(word, answer);
            }
        }
        return true;
    }

    public static void main(String[] args) {
        UserInterface game = new UserInterface();
        getHelp();
        game.run();
    }

    private static void getHelp() {
        System.out.println("Linguistic analizator v1. \n" +
                "The author is Tushar Brahmacobalol. \n" +
                "Type help for help.\n" +
                "Please, answer 'y' or 'n'");
    }

    private static void printResult(int vocabularySize) {
        System.out.println("Your estimated vocabulary is " + vocabularySize + " words");
    }

    private static void getErrorMessage() {
        System.out.println("Error while reading answer");
    }

    public void getQuestion(String word) {
        System.out.println("Do you know translation of this word?");
        System.out.println(word);
    }
}
