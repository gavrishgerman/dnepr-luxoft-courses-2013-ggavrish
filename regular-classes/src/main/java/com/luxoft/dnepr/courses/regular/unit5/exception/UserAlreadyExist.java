package com.luxoft.dnepr.courses.regular.unit5.exception;

public class UserAlreadyExist extends RuntimeException {
    public String getMessage() {
        return message;
    }

    private String message;
    public UserAlreadyExist(String message){
        this.message=message;
    }

    public  UserAlreadyExist(){}
}
