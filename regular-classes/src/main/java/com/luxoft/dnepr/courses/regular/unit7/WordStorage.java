package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    private Map<String,Number> wordStorage = new HashMap<>();
    private Map<String,? extends Number> unmodifMap = Collections.unmodifiableMap(wordStorage);

    /**
     * Saves given word and increments count of occurrences.
     * @param word    word
     */
    public synchronized void save(String word) {
        assert word!= null;
        if(wordStorage.containsKey(word)){
            int n = (int)wordStorage.get(word)+1;
            wordStorage.remove(word);
            wordStorage.put(word,n);
        }
        else wordStorage.put(word,1);
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return unmodifMap;
    }
}
