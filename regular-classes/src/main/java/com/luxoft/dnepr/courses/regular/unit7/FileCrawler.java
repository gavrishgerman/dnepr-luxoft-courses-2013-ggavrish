package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {
    private WordStorage wordStorage = new WordStorage();

    private String rootFolder;
    private int maxNumberOfThreads;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }
  /*
    public void getFiles(String directoryName) throws FileNotFoundException, InterruptedException {
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
          assert fList!=null;
        for (File file : fList) {
            if (file.isFile()&&file.getAbsolutePath().endsWith(FILE_FORMAT)) {
                getWords(file);
            } else if (file.isDirectory()) {
                getFiles(file.getAbsolutePath());
            }
        }
    }

    private void getWords(File file) throws FileNotFoundException, InterruptedException {

        files.add(file);
       Scanner in = new Scanner(new FileInputStream(file));
        in.useDelimiter(("[.,:;()*=-?!\"\\s]+"));
        while (in.hasNext()){
            wordStorage.save(in.next());
        }
    }

    */

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        BlockingQueue<File> que = new LinkedBlockingQueue<>();
        Producer producer = new Producer(rootFolder, que);

        List<File> files = new ArrayList<>();
        Consumer consumer = new Consumer(files, que, wordStorage);

        Thread[] threads = getArrayOfThreads(maxNumberOfThreads, producer, consumer);

        startThreads(threads);


        try {
            waitThreads(threads);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new FileCrawlerResults(files, wordStorage.getWordStatistics());
    }


    private void waitThreads(Thread[] threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    private void startThreads(Thread[] threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private Thread[] getArrayOfThreads(int maxNumberOfThreads, Producer producer, Consumer consumer) {
        Thread[] threads = new Thread[maxNumberOfThreads];
        threads[0] = new Thread(producer);
        for (int i = 0; i < maxNumberOfThreads - 1; i++) {
            threads[i + 1] = new Thread(consumer);
        }
        return threads;
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
