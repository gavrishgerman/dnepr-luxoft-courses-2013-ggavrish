package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

/**
 * User: Гера
 * Date: 23.11.13
 * Time: 14:16
 */
public class Consumer implements Runnable {
    private List<File> files;
    private BlockingQueue<File> queue;
    private WordStorage wordStorage;

    Consumer(List<File> files, BlockingQueue<File> queue, WordStorage wordStorage) {
        this.files = files;
        this.queue = queue;
        this.wordStorage = wordStorage;
    }

    public void run() {
          try {
            boolean isLast = false;
            while (!Thread.currentThread().isInterrupted() && !isLast) {
                File f = queue.take();
                if (f == Producer.STUB) {
                    queue.put(f);
                    isLast = true;
                } else {
                    getWords(f);
                }
            }
        } catch (InterruptedException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getWords(File file) throws FileNotFoundException {

        files.add(file);
        Scanner in = new Scanner(new FileInputStream(file));
        in.useDelimiter(("[.,:;()*=-?!\"\\s]+"));
        while (in.hasNext()) {
            wordStorage.save(in.next());
        }
    }
}
