package com.luxoft.dnepr.courses.unit2;


import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    /**
     * Method calculate word average length.
     *
     * @param str is sentence
     * @return average length of words
     */
    public static double wordAverageLength(String str) {
        if (str == null || str.equals("")) return 0;
        double sum = 0;
        String[] arr = stringToArrayString(str);
        for (String s : arr) {
            sum += s.length();
        }
        return sum / numberOfWords(str);
    }


    /**
     * Method converts String to String[].
     *
     * @param str is sentence.
     * @return array that contains str's words.
     */
    private static String[] stringToArrayString(String str) {
        String[] array = new String[numberOfWords(str)];
        for (int i = 0; i < array.length; i++) {
            array[i] = "";
        }
        int wordNumber = 0;
        for (int i = 0; i < str.length(); i++) {
            if (!str.substring(i, i + 1).equals(" ")) {
                array[wordNumber] += str.substring(i, i + 1);
            } else {
                wordNumber++;
            }
        }
        return array;
    }

    /**
     * Method calculate number of words in sentence.
     *
     * @param str is sentence.
     * @return number of words in sentence.
     */
    private static int numberOfWords(String str) {
        int number = 1;
        if (str == null || str.equals("")) return 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.substring(i, i + 1).equals(" ")) {
                number++;
            }
        }
        return number;

    }

    /**
     * Method reverse every single word in sentence.
     *
     * @param str is sentence of some words
     * @return sentence with reversed words.
     */
    public static String reverseWords(String str) {

        if (str == null || str.equals("")) return "";
        String[] arr = stringToArrayString(str);
        String result = "";
        for (int i = 0; i < arr.length - 1; i++) {
            result += reverse(arr[i]) + " ";
        }
        result += reverse(arr[arr.length - 1]);
        return result;
    }

    /**
     * Method reverse a single word.
     *
     * @param str which is word and
     * @return reversed one.
     */
    private static String reverse(String str) {
        StringBuilder builder = new StringBuilder(str);
        return builder.reverse().toString();
    }

    /**
     * Method takes
     *
     * @param str is combinations of symbols
     * @return char array ordered by max appearanced symbol, if equal => alphabetical order, uppercase first.
     */
    public static char[] getCharEntries(String str) {
        TreeMap<Character, Integer> hm = new TreeMap<Character, Integer>();

        for (int i = 0; i < str.length(); i++) {
            Character charI = str.charAt(i);
            if (!charI.equals('\u0020')) {
                hm.put(charI, hm.containsKey(charI) ? (hm.get(charI) + 1) : 1);
            }
        }

        List<Object> entryList = new ArrayList<Object>(hm.entrySet());
        Collections.sort(entryList, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                Map.Entry e1 = (Map.Entry) o1;
                Map.Entry e2 = (Map.Entry) o2;
                Comparable c1 = (Comparable) e1.getValue();
                Comparable c2 = (Comparable) e2.getValue();
                return c2.compareTo(c1);
            }
        });
        char[] arr = new char[entryList.size()];
        for (int l = 0; l < entryList.size(); l++) {
            arr[l] = entryList.get(l).toString().charAt(0);
        }
        return arr;
    }

    /**
     * Method calculates overall area.
     *
     * @param figures is list of figures.
     * @return overall area.
     */
    public static double calculateOverallArea(List<Figure> figures) {
        double sum = 0;
        for (Figure l : figures) {
            sum += l.calculateArea();
        }
        return sum;
    }

    /**
     * Sorting an array using mergesort algorithm.
     *
     * @param array an array of a-zA-z.
     * @param asc   type of sorting(increasing/decreasing).
     * @return sorted array.
     */
    public static String[] sortArray(String[] array, boolean asc) {
        if (array == null) return null;
        String[] clone = array.clone();
        mergeSort(clone, asc);
        return clone;
    }

    /**
     * * Mergesort algorithm.
     *
     * @param a   an array of Comparable items.
     * @param asc type of sorting(increasing/decreasing).
     */
    private static void mergeSort(Comparable[] a, boolean asc) {
        Comparable[] tmpArray = new Comparable[a.length];
        mergeSort(a, tmpArray, 0, a.length - 1, asc);
    }

    /**
     * * Internal method that makes recursive calls.
     *
     * @param a        an array of Comparable items.
     * @param tmpArray an array to place the merged result.
     * @param left     the left-most index of the subarray.
     * @param right    the right-most index of the subarray.
     * @param asc      type of sorting(increasing/decreasing).
     */
    private static void mergeSort(Comparable[] a, Comparable[] tmpArray,
                                  int left, int right, boolean asc) {
        if (left < right) {
            int center = (left + right) / 2;
            mergeSort(a, tmpArray, left, center, asc);
            mergeSort(a, tmpArray, center + 1, right, asc);
            merge(a, tmpArray, left, center + 1, right, asc);
        }
    }

    /**
     * Internal method that merges two sorted halves of a subarray.
     *
     * @param a        an array of Comparable items.
     * @param tmpArray an array to place the merged result.
     * @param leftPos  the left-most index of the subarray.
     * @param rightPos the index of the start of the second half.
     * @param rightEnd the right-most index of the subarray.
     * @param asc      type of sorting(increasing/decreasing).
     */
    private static void merge(Comparable[] a, Comparable[] tmpArray,
                              int leftPos, int rightPos, int rightEnd, boolean asc) {
        int leftEnd = rightPos - 1;
        int tmpPos = leftPos;
        int numElements = rightEnd - leftPos + 1;
        while (leftPos <= leftEnd && rightPos <= rightEnd)
            if (asc) {
                if (a[leftPos].compareTo(a[rightPos]) <= 0)
                    tmpArray[tmpPos++] = a[leftPos++];
                else
                    tmpArray[tmpPos++] = a[rightPos++];
            } else {
                if (a[leftPos].compareTo(a[rightPos]) > 0)
                    tmpArray[tmpPos++] = a[leftPos++];
                else
                    tmpArray[tmpPos++] = a[rightPos++];
            }

        while (leftPos <= leftEnd)
            tmpArray[tmpPos++] = a[leftPos++];
        while (rightPos <= rightEnd)
            tmpArray[tmpPos++] = a[rightPos++];
        for (int i = 0; i < numElements; i++, rightEnd--)
            a[rightEnd] = tmpArray[rightEnd];
    }
}
