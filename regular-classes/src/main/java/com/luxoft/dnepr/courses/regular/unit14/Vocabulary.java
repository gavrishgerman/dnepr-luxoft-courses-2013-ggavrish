package com.luxoft.dnepr.courses.regular.unit14;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Vocabulary {
    private List<String> vocabulary;

    public int getSize() {
        return vocabulary.size();
    }

    public Vocabulary() {
        vocabulary = new ArrayList<>();
        initVocabulary();
    }

    private void initVocabulary() {
        Scanner in = new Scanner(new InputStreamReader(getClass().getResourceAsStream("sonnets.txt")));
        in.useDelimiter("[.,:;()?!\"\\s\\d]+");
        while (in.hasNext()) {
            String word = in.next().toLowerCase();
            if (word.length() > 3 && !vocabulary.contains(word)) {
                vocabulary.add(word);
            }
        }
    }

    public String getRandomWord() {
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(vocabulary.size());
        return  vocabulary.get(index);
    }
}
