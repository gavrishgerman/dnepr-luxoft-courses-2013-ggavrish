package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {
    private double side;

    public Hexagon(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return Math.sqrt(3) * 3 / 2 * Math.pow(side, 2);
    }
}
