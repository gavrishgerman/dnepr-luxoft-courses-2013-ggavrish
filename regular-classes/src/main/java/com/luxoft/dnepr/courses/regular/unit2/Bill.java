package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<Product> list;
    private CompositeProduct cp = new CompositeProduct();

    Bill() {
        list = new ArrayList<>();
    }


    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {

        if (list.isEmpty()) {
            cp.add(product);
            list.add(cp);
        } else {

            if (product.equals(cp.getChild().get(0))) {
                cp.add(product);
            } else {
                cp = new CompositeProduct();
                cp.add(product);
                list.add(cp);
            }
        }

    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return total cost
     */
    public double summarize() {
        double sum = 0;
        for (Product l : list) {
            sum += l.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return sorted list in descending order
     */
    public List<Product> getProducts() {
        Collections.sort(list, new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                double price1 = o1.getPrice();
                double price2 = o2.getPrice();
                if (price1 > price2) {
                    return -1;
                }
                return 0;
            }
        });
        return list;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
