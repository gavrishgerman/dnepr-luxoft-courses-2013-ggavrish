package com.luxoft.dnepr.courses.regular.unit5.exception;

public class UserNotFound extends RuntimeException {
   private String message;

    public UserNotFound(){}
    public UserNotFound(String message){
        this.message=message;
    }
    public String getMessage() {
        return message;
    }
}
