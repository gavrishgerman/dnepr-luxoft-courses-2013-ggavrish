package com.luxoft.dnepr.courses.unit1;

/**
 * Created with IntelliJ IDEA.
 * User: German
 * Date: 04.10.13
 * Time: 11:31
 */

public final class LuxoftUtil {
    private LuxoftUtil() {
    }

    /**
     * this is just a test
     * method, provides hello message for user
     *
     * @param name is the user's name
     * @return "Hi, name"
     *         one should not use numbers in name, unless R2D2
     */
    public static String hello(String name) {
        if (name == null || name.equals("")) return "I don't know you";
        if (name.equals("R2D2")) return "Hi, R2D2";
        for (int i = 0; i < name.length() - 1; i++) {
            String letter = name.substring(i, i + 1);
            if ("1234567890".contains(letter)) return "Please, don't use numbers in your name, unless you are R2D2";
        }
        return "Hi, " + name;
    }

    /**
     * method indicates
     *
     * @param monthOrder and
     * @param language   is input language
     * @return month name depends on language and month selected
     */
    public static String getMonthName(int monthOrder, String language) {
        if (language.equals("ru"))
            return getMonthNameRu(monthOrder);

        if (language.equals("en"))
            return getMonthNameEng(monthOrder);

        return "Unknown language";
    }

    public static String getMonthNameRu(int monthOrder) {
        if (monthOrder > 0 || monthOrder <= 12)
            switch (monthOrder) {
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";

            }
        return "Неизвестный месяц";
    }

    public static String getMonthNameEng(int monthOrder) {
        if (monthOrder > 0 || monthOrder <= 12)
            switch (monthOrder) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";

            }
        return "Unknown month";
    }

    /**
     * method converts
     *
     * @param intNumber is integer input number and
     * @return binary number
     */
    public static String binaryToDecimal(String intNumber) {
        int powMultiplier = 0;
        int number = 0;
        for (int i = intNumber.length() - 1; i >= 0; i--) {
            String digit = intNumber.substring(i, i + 1);
            if (!digit.equals("1") && !digit.equals("0")) return "Not binary";
            if (digit.equals("1")) {
                number += Math.pow(2, powMultiplier);
            }
            powMultiplier++;
        }
        return number + "";
    }

    /**
     * method converts
     *
     * @param decimalNumber is decimal number and
     * @return binary number
     */
    public static String decimalToBinary(String decimalNumber) {
        int intNumber;
        String isNegative = "", binaryNumber = "";

        if (decimalNumber.substring(0, 1).equals("-")) {
            isNegative = "-";
            decimalNumber = decimalNumber.substring(1, decimalNumber.length());
        }
        try {
            intNumber = charArrayToInt(decimalNumber);
        } catch (NumberFormatException e) {
            return "Not decimal";
        }

        while (intNumber > 0) {
            binaryNumber = intNumber % 2 + binaryNumber;
            intNumber /= 2;
        }
        return isNegative + binaryNumber;
    }

    /**
     * method converts
     *
     * @param myString contains array of int
     * @return to int
     */
    static int charArrayToInt(String myString) throws NumberFormatException {
        char[] data = myString.toCharArray();
        int result = 0;
        for (char d : data) {
            int digit = (int) d - (int) '0';
            if ((digit < 0) || (digit > 9)) throw new NumberFormatException();
            result *= 10;
            result += digit;
        }
        return result;
    }

    /**
     * Method sorts
     *
     * @param array of integers depending on
     * @param asc   boolean parameter inc or dec
     * @return array
     */
    public static int[] sortArray(int[] array, boolean asc) {

        int[] sortedArray = array;

        if (asc) {
            for (int j = 1; j < sortedArray.length; j++) {
                for (int i = 0; i < sortedArray.length - 1; i++) {
                    if (sortedArray[i] > sortedArray[i + 1]) {
                        int temp = sortedArray[i];
                        sortedArray[i] = sortedArray[i + 1];
                        sortedArray[i + 1] = temp;
                    }

                }
            }
        } else {
            for (int j = 1; j < sortedArray.length; j++) {
                for (int i = 0; i < sortedArray.length - 1; i++) {
                    if (sortedArray[i] < sortedArray[i + 1]) {
                        int temp = sortedArray[i];
                        sortedArray[i] = sortedArray[i + 1];
                        sortedArray[i + 1] = temp;
                    }

                }
            }

        }
        return sortedArray;
    }
}
