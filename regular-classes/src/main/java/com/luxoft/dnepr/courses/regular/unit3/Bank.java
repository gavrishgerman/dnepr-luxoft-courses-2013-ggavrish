package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;
    private DecimalFormat decimalFormat;

    Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        String actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Illegal Java Version. Expected:" + actualJavaVersion);
        }
        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols();
        unusualSymbols.setDecimalSeparator('.');
        this.decimalFormat = new DecimalFormat(".00");
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setDecimalFormatSymbols(unusualSymbols);


    }

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        userCheck(fromUserId);
        userCheck(toUserId);

        walletCheck(fromUserId);
        walletCheck(toUserId);

        isEnoughMoney(fromUserId, amount);
        isEnoughLimit(toUserId, amount);

        getUsers().get(fromUserId).getWallet().withdraw(amount);
        getUsers().get(toUserId).getWallet().transfer(amount);
    }

    private void userCheck(Long user) throws NoUserFoundException {
        if (!getUsers().containsKey(user)) {
            throw new NoUserFoundException(user, "User '" + user + "' is not found.");
        }
    }

    private void walletCheck(Long user) throws TransactionException {
        if (getUsers().get(user).getWallet().getStatus() == WalletStatus.BLOCKED) {
            throw new TransactionException("User '" + user + "' wallet is blocked");
        }
    }

    private void isEnoughMoney(Long user, BigDecimal amount) throws TransactionException {

        try {
            getUsers().get(user).getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException | InsufficientWalletAmountException e) {
            throw new TransactionException("User '" + user + "' has insufficient funds (" + decimalFormat.format(getUsers().get(user).getWallet().getAmount()) + " < " + decimalFormat.format(amount) + ")");
        }
    }

    private void isEnoughLimit(Long user, BigDecimal amount) throws TransactionException {
        try {
            getUsers().get(user).getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException | LimitExceededException e) {
            throw new TransactionException("User '" + user + "' wallet limit exceeded (" + decimalFormat.format(getUsers().get(user).getWallet().getAmount()) + " + " + decimalFormat.format(amount) + " > " + decimalFormat.format(getUsers().get(user).getWallet().getMaxAmount()) + ")");
        }
    }
}
