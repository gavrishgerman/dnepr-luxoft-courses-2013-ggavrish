package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        return (childProducts.isEmpty()) ? null : childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        return (childProducts.isEmpty()) ? null : childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        if (getAmount() == 1) {
            return childProducts.get(0).getPrice();
        }
        if (getAmount() == 2) {
            double price = childProducts.get(0).getPrice() + childProducts.get(1).getPrice();
            return price * 0.95;
        }
        if (getAmount() >= 3) {
            double sum = 0;
            for (Product p : childProducts) {
                sum += p.getPrice();
            }
            return sum * 0.9;
        }
        return 0;

    }

    public List<Product> getChild() {
        return childProducts;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
