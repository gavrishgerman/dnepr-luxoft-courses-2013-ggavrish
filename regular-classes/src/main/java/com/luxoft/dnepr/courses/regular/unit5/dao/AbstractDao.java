package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

public abstract class AbstractDao<T extends Entity> implements IDao<T> {

    private Class<T> type;

    public AbstractDao(Class<T> type) {
        this.type = type;
    }

    private Map<Long, Entity> storage = EntityStorage.getEntities();

    @Override
    public T update(T model) {
        if (model.getId() == null || get(model.getId()) == null) {
            throw new UserNotFound();
        }
        Long elementId = model.getId();
        storage.put(elementId, model);
        return model;
    }

    public Long maxId() {
        Long maximum = 0L;
        for (Map.Entry<Long, Entity> entry : storage.entrySet()) {
            if (maximum.compareTo(entry.getKey()) <= 0) {
                maximum = entry.getKey();
            }
        }
        return maximum;
    }


    private void checkAlreadyExist(T entity) {
        if (storage.containsKey(entity.getId())) {
            throw new UserAlreadyExist("User already exist");
        }
    }

    @Override
    public T save(T entity) throws UserAlreadyExist {
        if (entity == null) {
            throw new IllegalArgumentException("null in save");
        }
        Long id;
        if (entity.getId() == null) {
            id = maxId() + 1;
        } else {
            id = entity.getId();
            checkAlreadyExist(entity);
        }
        storage.put(id, entity);
        entity.setId(id);
        return entity;
    }


    @Override
    public T get(long id) {
        return (T) storage.get(id);
    }

    @Override
    public boolean delete(long id) {
        return storage.remove(id) != null;
    }
}
