package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.concurrent.BlockingQueue;

/**
 * User: Гера
 * Date: 23.11.13
 * Time: 14:16
 */
public class Producer implements Runnable {
    private final static String FILE_FORMAT="txt";
    private BlockingQueue<File> queue;
    private String rootFolder;
    public static final File STUB = new File("");


    Producer(String rootFolder, BlockingQueue<File> queue){
        this.rootFolder=rootFolder;
        this.queue=queue;
    }

  public void run(){
                 getFiles(rootFolder,queue);
      try {
          queue.put(STUB);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
    }

    public static void getFiles(String directoryName, BlockingQueue<File> queue) {
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        assert fList!=null;
        for (File file : fList) {
            if (file.isFile()&&file.getAbsolutePath().endsWith(FILE_FORMAT)) {
                if(!queue.contains(file)){
                queue.add(file);         }
            } else if (file.isDirectory()) {
                getFiles(file.getAbsolutePath(),queue);
            }
        }
    }
}
