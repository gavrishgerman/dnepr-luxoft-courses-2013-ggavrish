package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {
    private Date date;

    public Date getPublicationDate() {
        return date;
    }

    public void setPublicationDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (date != null ? !date.equals(book.date) : book.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book book = (Book) super.clone();
        book.setPublicationDate(new Date(this.date.getTime()));
        return book;
    }
}
