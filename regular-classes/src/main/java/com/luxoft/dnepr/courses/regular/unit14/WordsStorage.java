package com.luxoft.dnepr.courses.regular.unit14;

import java.util.ArrayList;
import java.util.List;

public class WordsStorage {
    public static List<String> knownWords = new ArrayList<>();
    public static List<String> unknownWords = new ArrayList<>();

    public static int getResults(int vocabularySize) {
        return vocabularySize * (knownWords.size() + 1) / (knownWords.size() + unknownWords.size() + 1);
    }

    public static void answer(String word, Boolean answer) {
        if (answer) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }

}
