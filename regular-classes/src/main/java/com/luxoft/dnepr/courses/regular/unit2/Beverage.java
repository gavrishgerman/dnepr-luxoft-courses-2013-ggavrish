package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
    private boolean isNonAlcoholic;

    public boolean isNonAlcoholic() {
        return isNonAlcoholic;
    }

    public void setNonAlcoholic(boolean alc) {
        isNonAlcoholic = alc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (isNonAlcoholic != beverage.isNonAlcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (isNonAlcoholic ? 1 : 0);
        return result;
    }
}
