package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException{
        if(getStatus()==WalletStatus.BLOCKED){
            throw new WalletIsBlockedException(getId(),"Cannot withdrew, wallet's status is blocked");
        }
        if (getAmount().subtract(amountToWithdraw).compareTo(BigDecimal.ZERO)<0){
            throw new InsufficientWalletAmountException(getId(),amountToWithdraw,getAmount(),"Insufficient wallet's funds");
        }
            }

    public void withdraw(BigDecimal amountToWithdraw){
        setAmount(getAmount().subtract(amountToWithdraw));
    }
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException{
        if(getStatus()==WalletStatus.BLOCKED){
            throw new WalletIsBlockedException(getId(),"Wallet is blocked during checkTransfer");
        }
        if(getAmount().add(amountToTransfer).compareTo(getMaxAmount())>0){
            throw new LimitExceededException(getId(),amountToTransfer,getAmount(),"Cannot transfer, max wallet amount reached");
        }
    }
    public  void transfer(BigDecimal amountToTransfer){
        setAmount(getAmount().add(amountToTransfer));
    }
}
