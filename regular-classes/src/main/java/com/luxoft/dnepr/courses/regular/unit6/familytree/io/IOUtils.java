package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Scanner;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) {
        try {
            return load(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    /**
     * PARSER JSON
     * This is get very serious. Parser based on recursion.
     *
     * @param scanner is input stream
     * @return person with all available fields
     */
    private static PersonImpl parser(Scanner scanner) {
        PersonImpl person = new PersonImpl();
        while (scanner.hasNext()) {
            String[] el = scanner.next().replaceAll("\"", "").split(":");
            if (el.length > 1) {
                set(person, el[0], el[1]);
            } else {
                switch (el[0]) {
                    case "father":
                        person.setFather(parser(scanner));
                        continue;
                    case "mother":
                        person.setMother(parser(scanner));
                        continue;
                }
                return person;
            }
        }
        return person;
    }

    /**
     * Method that helps add to the person fields.
     *
     * @param person we want to change field.
     * @param name   is the name of the field.
     * @param value  is the value of the field.
     */
    public static void set(PersonImpl person, String name, String value) {
        switch (name) {
            case "name":
                person.setName(value);
                break;
            case "ethnicity":
                person.setEthnicity(value);
                break;
            case "gender":
                person.setGender(Gender.valueOf(value));
                break;
            case "age":
                person.setAge(Integer.parseInt(value));
                break;
        }
    }

    public static FamilyTree load(InputStream is) {
        Scanner scanner = new Scanner(is);
        PersonImpl person = read(scanner);
        return FamilyTreeImpl.create(person);
    }

    /**
     * Small scanner manipulation before parser goes in. In other words skipping "root"
     * and delimiter use
     *
     * @param scanner is scanner stream
     * @return person
     */
    private static PersonImpl read(Scanner scanner){
        scanner.useDelimiter("[{},]");
        scanner.next();
        return parser(scanner);
    }


    public static void save(String filename, FamilyTree familyTree) {
        try {
            save(new FileOutputStream(filename), familyTree);
        } catch (FileNotFoundException e) {
        }
    }

    /**
     * JSON compiler.
     * Create string in <a href="http://en.wikipedia.org/wiki/JSON">JSON</a> format.
     *
     * @param familyTree is family tree of object.
     * @return string in JSON format, all in 1 line.
     * @throws IllegalAccessException
     */
    private static String saveJSON(Object familyTree) throws IllegalAccessException {
        StringBuilder stringJSON = new StringBuilder();
        stringJSON.append("{");
        Field[] privateFields = familyTree.getClass().getDeclaredFields();
        for (Field field : privateFields) {
            field.setAccessible(true);
            if (field.get(familyTree) != null && !Modifier.isTransient(field.getModifiers())) {
                stringJSON.append("\"").append(field.getName()).append("\":");
                stringJSON.append("\"").append(field.get(familyTree)).append("\"");
                if (field.get(familyTree) instanceof Person) {
                    stringJSON.delete(stringJSON.length() - 2, stringJSON.length());//delete "" before {
                    stringJSON.append(saveJSON(field.get(familyTree)));
                }
                stringJSON.append(",");
            }
        }
        stringJSON.delete(stringJSON.length() - 1, stringJSON.length()).append("}");
        return stringJSON.toString();
    }
    public static void save(OutputStream os, FamilyTree familyTree) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(os);
            String stringJSON = "{\"root\": " + saveJSON(familyTree.getRoot()) + "}";
            osw.write(stringJSON);
            osw.flush();
        } catch (IOException | IllegalAccessException e) {
        }

    }
}
