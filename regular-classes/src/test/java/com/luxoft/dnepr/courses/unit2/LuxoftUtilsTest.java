package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;

import org.junit.Test;
import org.junit.Assert;


import java.util.ArrayList;
import java.util.List;

public class LuxoftUtilsTest {
    private final double DELTA = 0.000001;


    @Test
    public void testWordAverageLength() throws Exception {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), DELTA);
        Assert.assertEquals(4.6, LuxoftUtils.wordAverageLength("Liberate tute me ex inferis"), DELTA);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), DELTA);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(null), DELTA);
    }

    @Test
    public void testReverseWords() {
        Assert.assertEquals("London is a capital of Great Britan", LuxoftUtils.reverseWords("nodnoL si a latipac fo taerG natirB"));
        Assert.assertEquals("cba", LuxoftUtils.reverseWords("abc"));
        Assert.assertEquals("  cba    trf rgrg  ", LuxoftUtils.reverseWords("  abc    frt grgr  "));
    }

    @Test
    public void testReverseWordsError() {
        Assert.assertEquals("", LuxoftUtils.reverseWords(null));
        Assert.assertEquals("", LuxoftUtils.reverseWords(""));
    }

    @Test
    public void testGetCharEntries() {
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
        Assert.assertArrayEquals(new char[]{'a', 'I', 'c', 'e', 'h', 't', 'v'}, LuxoftUtils.getCharEntries("I have a cat"));
    }

    @Test
    public void testCalculateOverallArea() {
        List<Figure> l = new ArrayList<>();
        l.add(new Square(2));
        l.add(new Hexagon(2));
        l.add(new Circle(1));
        Assert.assertEquals(17.5338974990, LuxoftUtils.calculateOverallArea(l), DELTA);
    }

    @Test
    public void testSortArray() {
        String[] testArray = {"b", "d", "c", "a", "e", "Z", "A"};
        String[] expectedArrayTrue = {"A", "Z", "a", "b", "c", "d", "e"};
        String[] expectedArrayFalse = {"e", "d", "c", "b", "a", "Z", "A"};
        Assert.assertArrayEquals(expectedArrayFalse, LuxoftUtils.sortArray(testArray, false));
        Assert.assertArrayEquals(expectedArrayTrue, LuxoftUtils.sortArray(testArray, true));
    }

}