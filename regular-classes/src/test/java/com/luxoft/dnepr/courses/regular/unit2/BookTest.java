package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        assertEquals(book, cloned);
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        if (!book.equals(book2)) fail("Objects are not equal");
        book2.setCode("new code");
        if (book.equals(book2)) fail("Objects are not equal");
    }
}
