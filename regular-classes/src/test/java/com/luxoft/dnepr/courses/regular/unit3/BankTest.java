package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.fail;

public class BankTest {
    final static BigDecimal AMOUNT_INS = new BigDecimal(60.1);
    final static BigDecimal AMOUNT_LIMIT_EX = new BigDecimal(50.00432432432423);
    @Test
    public void testBankConstructor() throws Exception {
        try {
            String actualJavaVersion = System.getProperty("java.version");
            Bank bank = new Bank(actualJavaVersion);
            bank.getUsers();

        } catch (IllegalJavaVersionError e) {
            fail(e.getMessage());
        }
    }

    private Wallet createWallet(Long id, BigDecimal amount, BigDecimal maxAmount, WalletStatus status) {
        Wallet wallet = new Wallet();
        wallet.setId(id);
        wallet.setStatus(status);
        wallet.setMaxAmount(maxAmount);
        wallet.setAmount(amount);
        return wallet;
    }

    @Test
    public void testCheckWithdrawal() throws Exception{

    }

    @Test
    public void testMakeMoneyTransaction() throws Exception {

        User u1 = new User();
        u1.setId(1L);
        u1.setName("Pupkin");
        u1.setWallet(createWallet(1L,new BigDecimal(50.343434232423),new BigDecimal(100),WalletStatus.ACTIVE));

        User u3 = new User();
        u3.setId(3L);
        u3.setName("Egorov");
        u3.setWallet(createWallet(3L,new BigDecimal(46.3),new BigDecimal(49),WalletStatus.BLOCKED));


        User u2 = new User();
        u2.setId(2L);
        u2.setName("Egorov");
        u2.setWallet(createWallet(2L,new BigDecimal(30),new BigDecimal(60),WalletStatus.ACTIVE));

        User secretUser = new User();
        secretUser.setName("Hidden");
        secretUser.setId(4L);

        Map<Long, UserInterface> users = new HashMap<>();
        users.put(u1.getId(), u1);
        users.put(u2.getId(), u2);
        users.put(u3.getId(), u3);




        String actualJavaVersion = System.getProperty("java.version");
        Bank bank = new Bank(actualJavaVersion);

        bank.setUsers(users);

        try {
            bank.makeMoneyTransaction(u1.getId(), secretUser.getId(), AMOUNT_INS);
        } catch (NoUserFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            bank.makeMoneyTransaction(u1.getId(), u3.getId(), AMOUNT_INS);
        } catch (TransactionException e) {
            System.out.println(e.getMessage());
        }
        try {
            bank.makeMoneyTransaction(u1.getId(), u2.getId(), AMOUNT_INS);
        } catch (TransactionException e) {
            System.out.println(e.getMessage());
        }
        try {
            bank.makeMoneyTransaction(u1.getId(), u2.getId(), AMOUNT_LIMIT_EX);
        } catch (TransactionException e) {
            System.out.println(e.getMessage());
        }


    }
}
