package com.luxoft.dnepr.courses.regular.unit5.storage;


import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Test;

import static junit.framework.Assert.*;

public class EntityStorageTest {
    private IDao<Employee> empl = new EmployeeDaoImpl();

    @Test
    public void saveIdNullTest() {
        Employee emp = new Employee();
        emp.setId(null);
        try {
            empl.save(emp);
        } catch (UserAlreadyExist e) {
        }
        assertTrue(EntityStorage.getEntities().containsKey(101L));
    }

    @Test
    public void saveUserAlreadyExistTest() {
        Employee secondEmployee = new Employee();
        secondEmployee.setId(1L);
        try {
            empl.save(secondEmployee);
        } catch (UserAlreadyExist e) {
            assertEquals("User already exist", e.getMessage());
        }
    }

    @Test
    public void normalSaveTest() {
        Employee employee = new Employee();
        employee.setId(99L);
        try {
            empl.save(employee);
        } catch (UserAlreadyExist e) {
            fail();
        }
        assertTrue(EntityStorage.getEntities().containsKey(99L));
    }


    @Test
    public void getTest() {
        Employee employee = new Employee();
        employee.setId(100L);
        employee.setSalary(100);

        try {
            empl.save(employee);
        } catch (UserAlreadyExist e) {

        }
        assertEquals(100, empl.get(100L).getSalary());

    }

    @Test
    public void getNullTest() {
        assertEquals(null, empl.get(2321321L));
    }

    @Test
    public void UpdateTest() {
        Employee newEmployee = new Employee();
        newEmployee.setId(100L);
        newEmployee.setSalary(200);
        try {
            empl.update(newEmployee);
        } catch (UserNotFound e) {

        }

        assertEquals(200, empl.get(100L).getSalary());
    }

    @Test
    public void deleteTest() {
        assertFalse(empl.delete(123213L));
        assertTrue(empl.delete(100L));
    }

    @Test
    public void redisDaoTest() {
        IDao<Redis> redisIDao = new RedisDaoImpl();
        try {
            redisIDao.save(new Redis(8L, 20));
        } catch (UserAlreadyExist e) {

        }
        assertTrue(redisIDao.delete(8L));

    }


}

