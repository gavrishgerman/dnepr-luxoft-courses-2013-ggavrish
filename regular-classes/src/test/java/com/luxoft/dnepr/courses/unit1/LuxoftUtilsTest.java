package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;


/**
 * Created with IntelliJ IDEA.
 * User: German
 * Date: 04.10.13
 * Time: 11:30
 */
public class LuxoftUtilsTest {
    @Test
    public void testHello() {
        Assert.assertEquals("Hi, Mike", LuxoftUtil.hello("Mike"));
        Assert.assertEquals("Hi, Jimmy", LuxoftUtil.hello("Jimmy"));
        Assert.assertEquals("Hi, Hugo", LuxoftUtil.hello("Hugo"));
        Assert.assertEquals("Hi, Elvin", LuxoftUtil.hello("Elvin"));
        Assert.assertEquals("Hi, John", LuxoftUtil.hello("John"));
    }

    @Test
    public void testHelloErrors() {
        Assert.assertEquals("I don't know you", LuxoftUtil.hello(null));
        Assert.assertEquals("I don't know you", LuxoftUtil.hello(""));
        Assert.assertEquals("Hi, R2D2", LuxoftUtil.hello("R2D2"));
        Assert.assertEquals("Please, don't use numbers in your name, unless you are R2D2", LuxoftUtil.hello("Jeronimo123"));
    }

    @Test
    public void testGetMonthName() {
        Assert.assertEquals("July", LuxoftUtil.getMonthName(7, "en"));
        Assert.assertEquals("Июнь", LuxoftUtil.getMonthName(6, "ru"));
    }

    @Test
    public void testGetMonthNameErrors() {
        Assert.assertEquals("Unknown language", LuxoftUtil.getMonthName(7, "ру"));
        Assert.assertEquals("Unknown month", LuxoftUtil.getMonthName(-7, "en"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtil.getMonthName(13, "ru"));
        Assert.assertEquals("Unknown language", LuxoftUtil.getMonthName(13, "hindi"));

    }

    @Test
    public void testbinaryToDecimal() {
        Assert.assertEquals("182", LuxoftUtil.binaryToDecimal("10110110"));
        Assert.assertEquals("34", LuxoftUtil.binaryToDecimal("100010"));
    }

    @Test
    public void testbinaryToDecimalError() {
        Assert.assertEquals("Not binary", LuxoftUtil.binaryToDecimal("777"));
        Assert.assertEquals("Not binary", LuxoftUtil.binaryToDecimal("bla-bla-bla"));
    }

    @Test
    public void testDecimalToBinary() {
        Assert.assertEquals("10111000", LuxoftUtil.decimalToBinary("184"));
        Assert.assertEquals("100010", LuxoftUtil.decimalToBinary("34"));
        Assert.assertEquals("10110111", LuxoftUtil.decimalToBinary("183"));
        Assert.assertEquals("-1100011", LuxoftUtil.decimalToBinary("-99"));
    }

    @Test
    public void testDecimalToBinaryError() {
        Assert.assertEquals("Not decimal", LuxoftUtil.decimalToBinary("blablabla"));
        Assert.assertEquals("Not decimal", LuxoftUtil.decimalToBinary("12345g"));
    }

    @Test
    public void testSortArray() {
        int[] arr = {8, 1, 4, 2, 3, 5, 6, 7, 9};
        int[] increasedArr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] decreasedArr = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        Assert.assertArrayEquals(increasedArr, LuxoftUtil.sortArray(arr, true));
        Assert.assertArrayEquals(decreasedArr, LuxoftUtil.sortArray(arr, false));
    }
}
