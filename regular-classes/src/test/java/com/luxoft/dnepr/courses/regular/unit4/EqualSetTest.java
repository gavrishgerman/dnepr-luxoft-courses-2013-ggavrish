package com.luxoft.dnepr.courses.regular.unit4;


import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

public class EqualSetTest {
    public EqualSetTest() {
    }


    @Test
    public void addNullTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        assertFalse(equalSet.add(null));
    }

    @Test
    public void addTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("abs");
        assertFalse(equalSet.add("abs"));
    }

    @Test
    public void addNullAndNotNullTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        equalSet.add("abs");
        equalSet.add(null);
        assertFalse(equalSet.add(null));
        assertFalse(equalSet.add("abs"));
    }


    @Test
    public void removeTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("abs");
        equalSet.add(null);
        assertTrue(equalSet.remove("abs"));
        assertTrue(equalSet.remove(null));
    }

    @Test
    public void constructorTest() {
        List<String> collection = new ArrayList<>();
        collection.add("mama, amma criminal");
        Set<String> equalSet = new EqualSet<>(collection);
        assertFalse(equalSet.add("mama, amma criminal"));
    }

    @Test
    public void constructorWithParamTest() {
        List<String> equalSet = new ArrayList<>();
        equalSet.add("first");
        equalSet.add("first");
        equalSet.add("third");
        equalSet.add("second");
        equalSet.add("second");
        equalSet.add("second");
        equalSet.add("third");
        equalSet.add("third");
        EqualSet<String> testSet = new EqualSet<>(equalSet);
        assertEquals(3, testSet.size());
    }

    @Test
    public void sizeTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        equalSet.add(null);
        equalSet.add("Hello");
        equalSet.add("Hello");
        assertEquals(2, equalSet.size());
    }

    @Test
    public void differentClassesTest() {
        Set<Integer> equalSet = new EqualSet<>();
        equalSet.add(1);
        equalSet.add(null);
        assertEquals(2, equalSet.size());
    }

    @Test
    public void addAllTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        equalSet.add("Hello");

        Set<String> equalSet2 = new EqualSet<>();
        equalSet2.add(null);
        equalSet2.add("Hello");
        equalSet2.add("Hi");

        assertTrue(equalSet.addAll(equalSet2));
        assertEquals(3, equalSet.size());
        assertFalse(equalSet.addAll(equalSet2));
    }


    @Test
    public void toArrayTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("Hello");
        equalSet.add(null);
        String[] arraySet = equalSet.toArray(new String[equalSet.size()]);
        assertEquals(arraySet[0], "Hello");
        assertEquals(arraySet[1], null);
    }

    @Test
    public void removeAllTest() {
        EqualSet<String> equalSet = new EqualSet<>();
        equalSet.add("first");
        equalSet.add("second");
        equalSet.add("third");

        EqualSet<String> set = new EqualSet<>();
        set.add("second");
        set.add("third");
        set.add("4-th");

        equalSet.removeAll(set);
        assertEquals(1, equalSet.size());
    }
}
