var showcards = true;
$(document).ready(function () {
    $('#hit').click(function () {
        $.ajax({
            url: "http://localhost:8081/service",
            data: {
                method: 'requestMore'
            }
        }).done(function (response) {
                console.log("Received from server:", response);
                var root = $.parseJSON(response);

                if (showcards) {
                    showNewCards(root.myhand, "#player_place");
                    $("#playerTotal").text(root.total);
                }

                if (root.total > 21) {
                    loose();
                    showcards = false;
                }

            });
    });
    $('#stay').click(function () {
        $.ajax({
            url: "http://localhost:8081/service",
            data: {
                method: 'stand'
            }
        }).done(function (response) {
                console.log("Received from server:", response);
                var root = $.parseJSON(response);

                if(showcards){
                $("#dealerTotal").text(root.total);
                showNewCards(root.dealershand, "#dealer_place");
                $('#win').text(root.winstate);
                }
                showcards=false;
            });
    });
    $('#newGame').click(function () {
        $.ajax({
            url: "http://localhost:8081/service",
            data: {
                method: 'newGame'
            }
        }).done(function (response) {
                console.log("Received from server:", response);
                var root = $.parseJSON(response);
                showcards = true;
                $('#win').text("Let's play some");
                clearHands();
                $("#dealerTotal").text(root.dealerstotal, '#dealer_text h3');
                $("#playerTotal").text(root.mytotal, '#player_text h3');
                showNewCards(root.myhand, "#player_place");
                showNewCards(root.dealershand, "#dealer_place");


            });
    });
    function clearHands() {
        $('#dealer_place').empty();
        $('#player_place').empty();
    }

    function loose() {
        $('#win').text("LOOSE");
    }

    function showNewCards(cards, place) {
        for (var i = 0; i < cards.length; i++) {
            $(place).append(" " + cards[i].suit + " " + cards[i].rank);
            //$('<img src="img/' + cards[i].rank + cards[i].suit + '.png">').css('float', 'left').appendTo($(place));
        }
    }
});