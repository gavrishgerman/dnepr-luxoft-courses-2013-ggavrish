package com.luxoft.dnepr.courses.unit3.view;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class CommandLineInterface {

    private static final String LOSE_MSG = "Sorry, today is not your day. You loose.";
    private static final String PUSH_MSG = "Push. Everybody has equal amount of points.";
    private static final String WIN_MSG = "Congrats! You win!";
    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    /**
     * Method provides playing the game of BlackJack.
     */
    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: (Gavrish German)\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();
        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }


    }

    /**
     * Main method in the game.
     *
     * @param command is what player says.
     * @param controller  is the main controller.
     * @return false if game is over.
     */
    private boolean execute(String command, GameController controller) {
        switch (command) {
            case Command.EXIT:
                return false;
            case Command.HELP:
                getHelp();
                break;
            case Command.MORE:
                return getMore(controller);
            case Command.STOP:
                return stop(controller);
            default:
                output.println("Invalid command");
        }
        return true;
    }

    /**
     * Method is letting a player take 1 more card.
     *
     * @param controller  is the main controller.
     * @return false if total cost of card is bigger than 21.
     */
    private boolean getMore(GameController controller) {
        controller.requestMore();
        printState(controller);
        List<Card> myHand = controller.getMyHand();
        if (controller.getCost(myHand) > 21) {
            output.println();
            finishMessage(controller);
            return false;
        }
        return true;
    }

    /**
     * Last method that has been called before game is over.
     *
     * @param controller is the main controller.
     * @return false every time, because game is over and nothing to do anymore.
     */
    private boolean stop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:");
        output.println();
        printState(controller);
        output.println();
        finishMessage(controller);
        return false;
    }

    /**
     * Method prints final message. Only being called, when the game is over.
     *
     * @param controller is the main controller.
     */
    private void finishMessage(GameController controller) {
        WinState winState = controller.getWinState();
        switch (winState) {
            case WIN:
                output.println(WIN_MSG);
                break;
            case LOOSE:
                output.println(LOSE_MSG);
                break;
            case PUSH:
                output.println(PUSH_MSG);
                break;
        }
    }

    /**
     * Prints help message.
     */
    private void getHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    /**
     * Prints cards player(or dealer) has in following format:
     * A 9 2 (total 21)
     *
     * @param hand is collection of card.
     * @param controller is the main controller.
     */
    private void format(List<Card> hand, GameController controller) {
        int total = controller.getCost(hand);

        for (Card aHand : hand) {
            output.print(aHand.getValues()+" ");
        }
        output.println("(total " + total + ")");

    }

    /**
     * Prints game's condition for player and dealer, for example
     * A 2 9 (total 22)
     * K (total 10)
     *
     * @param controller is the main controller.
     */
    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand, controller);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand, controller);
    }

}
