package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameController {
    private static GameController controller;
    private List<Card> deck;
    private List<Card> player;
    private List<Card> diller;

    private GameController() {
        this.deck = Deck.createDeck(1);
        player = new ArrayList<>();
        diller = new ArrayList<>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {

        newGame(new Shuffler() {
            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * clears all hands.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler shuffle interface
     */
    void newGame(Shuffler shuffler) {
        player.clear();
        diller.clear();
        shuffler.shuffle(this.deck);
        player.add(this.deck.remove(0));
        player.add(this.deck.remove(0));
        diller.add(this.deck.remove(0));

    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        int cost = getCost(player);
        if (cost <= 21) {
            player.add(deck.remove(0));
            cost = getCost(player);
        }
        return cost <= 21;
    }

    /**
     * Method sums up total value of hand.
     *
     * @param deck is list of cards, needs to be calculated.
     * @return total value of the hand.
     */
    public int getCost(List<Card> deck) {
        int cost = 0;
        for (Card d : deck) {
            cost += d.getCost();
        }
        return cost;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (getCost(diller) < 17) {
            diller.add(deck.remove(0));
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        if (getCost(player) > 21) return WinState.LOOSE;
        if (getCost(player) < getCost(diller) && getCost(diller) <= 21) return WinState.LOOSE;
        if (getCost(player) == getCost(diller)) return WinState.PUSH;
        return WinState.WIN;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return player;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return diller;
    }
}


