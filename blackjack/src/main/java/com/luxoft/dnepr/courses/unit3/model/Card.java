package com.luxoft.dnepr.courses.unit3.model;

public class Card {
	 private Rank rank;
    private Suit suit;
	public Card(Rank rank, Suit suit) {
		this.rank=rank;
        this.suit=suit;
	}

	public Rank getRank() {
		return rank;
	}

    public String getValues(){
        Rank r = rank;
        return r.getValues();
    }
	
	public int getCost() {
        Rank r = rank;
		return r.getCost();
	}

    public Suit getSuit() {
        return suit;
    }
}
