package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import java.util.ArrayList;
import java.util.List;

public final class Deck {
    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        if (size > 10) size = 10;
        if (size < 0) size = Math.abs(size);
        List<Card> list = new ArrayList<>(size * 52);
        for (int deckN = 0; deckN < size; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    list.add(new Card(rank, suit));
                }
            }
        }
        return list;
    }

    public static int costOf(List<Card> hand) {
        int sum = 0;
        for (Card aHand : hand) {
            sum += aHand.getCost();
        }
        return sum;
    }
}
